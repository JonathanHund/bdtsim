# Blockchain Data Trading Simulator (BDTsim)

## Quickstart

## Documentation

The full documentation can be found here: https://matthiaslohr.gitlab.io/bdtsim/

Quick Links:

  * [Quickstart Guide](https://matthiaslohr.gitlab.io/bdtsim/quickstart/)
  * [Command Documentation](https://matthiaslohr.gitlab.io/bdtsim/commands/)
  * [Protocol Documentation](https://matthiaslohr.gitlab.io/bdtsim/protocols/)
  * [Environment Documentation](https://matthiaslohr.gitlab.io/bdtsim/environments/)
  * [Data Provider Documentation](https://matthiaslohr.gitlab.io/bdtsim/data_providers/)
  * [Output Format Documentation](https://matthiaslohr.gitlab.io/bdtsim/output_formats/)


## Contributing

See the [Contribution Guidelines](https://gitlab.com/MatthiasLohr/bdtsim/-/blob/master/CONTRIBUTING.md) for information on how to contribute to this project.


## License

This project is published under the Apache License, Version 2.0.
See [LICENSE.md](https://gitlab.com/MatthiasLohr/bdtsim/-/blob/master/LICENSE.md) for more information.

Copyright (c) by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;

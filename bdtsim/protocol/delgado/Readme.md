# Delgado

The directory `delgado-trade` contains a Solidity implementation of the work by

> Delgado-Segura, Sergi, et al. "A fair protocol for data trading based on Bitcoin transactions." Future Generation Computer Systems (2017).

